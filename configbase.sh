#!/usr/bin/env bash
# filename: configbase.sh
####################################
### abinsur<at>cryptolab<dot>net ###
####################################



#---colors
DF="\033[0;39m"   #default
R="\033[1;31m"    #red
G="\033[1;32m"    #green
Y="\033[1;33m"    #yellow
B="\033[1;34m"    #blue
C="\033[0;36m"    #cyan



#---ctrl+c
#trap ctrl_c INT
#function ctrl_c(){
#	echo -e $B"Ctrl+C happened"$DF
#	echo
#}



#---only root
if [[ ${EUID} -ne 0 ]]; then
    echo -e $R"it as to be root"$DF
    echo
    exit
fi



#---untar
tar -xf config.tar



#---my user
XUSER="archer"



#---localization
ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
echo -e $Y"localtime done"$DF
echo
sleep 1



#---client timedatectl
echo -e $Y"timedatectl updating..."$DF
echo
sleep 1
cp -fv /etc/systemd/timesyncd.conf /etc/systemd/timesyncd.conf.aux
#sed -i '14c\[TIME]' /etc/systemd/timesyncd.conf
sed -i '20c\NTP=pool.ntp.br' /etc/systemd/timesyncd.conf
#sed -i '21c\FallbackNTP=a.ntp.br b.ntp.br c.ntp.br' /etc/systemd/timesyncd.conf
sed -i '21s/.//' /etc/systemd/timesyncd.conf
sed -i '22s/.//' /etc/systemd/timesyncd.conf
sed -i '23s/.//' /etc/systemd/timesyncd.conf
sed -i '24s/.//' /etc/systemd/timesyncd.conf
sed -i '25s/.//' /etc/systemd/timesyncd.conf
sed -i '26s/.//' /etc/systemd/timesyncd.conf
hwclock --systohc
systemctl enable systemd-timesyncd
echo -e $Y"/etc/systemd/timesyncd.conf done"$DF
chmod 0644 /etc/systemd/timesyncd.conf
echo
sleep 1



sed -i '387s/.//' /etc/locale.gen
locale-gen
echo "LANG=pt_BR.UTF-8" > /etc/locale.conf
export LANG=pt_BR.UTF-8
echo -e $Y"locale done"$DF
echo
sleep 1



cat > /etc/vconsole.conf << "AUX"
KEYMAP=br-abnt2
FONT=LatGrkCyr-8x16
AUX
echo -e $Y"keymap has done"$DF
echo
sleep 1


#---hostname and hosts
echo "${XUSER}" >> /etc/hostname
echo
sleep 1

echo "127.0.0.1	localhost" >> /etc/hosts
echo "127.0.0.1	${XUSER}.aux ${XUSER}" >> /etc/hosts
echo -e $Y"hostname and hosts has done"$DF
echo
sleep 1



#---pacman.conf
cp -fv /etc/pacman.conf /etc/pacman.conf.aux
sed -i '12s/.//' /etc/pacman.conf
sed -i '13s/.//' /etc/pacman.conf
sed -i '14s/.//' /etc/pacman.conf
sed -i '15s/.//' /etc/pacman.conf
sed -i '16s/.//' /etc/pacman.conf
sed -i '17s/.//' /etc/pacman.conf
sed -i '21s/.//' /etc/pacman.conf
#sed -i '18s/.//' /etc/pacman.conf
sed -i '33s/.//' /etc/pacman.conf
#sed -i '35s/.//' /etc/pacman.conf
sed -i '37c\ParallelDownloads = 9' /etc/pacman.conf
sed -i '37a\ILoveCandy' /etc/pacman.conf
echo -e $Y"/etc/pacman.conf done"$DF
chmod 0644 /etc/pacman.conf
echo
sleep 1



#---passwd root (password IS toor)
echo root:toor | sudo chpasswd
echo -e $B"the root passwd is 'toor'"$DF
echo
sleep 6



#---add user and sudoers
useradd -m -G wheel -s /bin/bash ${XUSER}
echo ${XUSER}:${XUSER} | sudo chpasswd
echo -e $B"the ${XUSER} passwd is '${XUSER}'"$DF
echo
sleep 6

install -v --directory -m 0755 -o root -g root /etc/sudoers.d
echo -e $Y"make directory /etc/sudoers.d"$DF
echo
sleep 1

> /etc/sudoers.d/${XUSER}
echo "${XUSER} ALL=(ALL) ALL" >> /etc/sudoers.d/${XUSER}
echo "${XUSER} ALL=(root) NOPASSWD: /local/file.ext" >> /etc/sudoers.d/${XUSER}
echo "${XUSER} ALL = NOPASSWD: /usr/bin/shutdown" >> /etc/sudoers.d/${XUSER}
chmod -v 0440 /etc/sudoers.d/${XUSER}
visudo -c
echo -e $Y"/etc/sudoers.d/${XUSER} ok"$DF
echo
sleep 1



#---ssh config
install -v --directory -m 0755 -o ${XUSER} -g ${XUSER} /home/${XUSER}/.ssh
cp -v ssh-config /home/${XUSER}/.ssh/config
chmod -v 0644 /home/${XUSER}/.ssh/config
chown -v ${XUSER}:${XUSER} /home/${XUSER}/.ssh/config

#--linux-rt-lts
#--update and install linux
#echo -e $Y"updating data base and install linux..."$DF
#pacman -Syu --needed --noconfirm linux-rt-lts
#echo -e $Y"updating data base and linux-rt-lts is done"$DF



#---hooks
pacman -S --needed --noconfirm coreutils pacman-contrib
install -v --directory -m 0755 -o root -g root /etc/pacman.d/hooks
cp -rv hooks/*.hook /etc/pacman.d/hooks
chmod -v 0644 /etc/pacman.d/hooks/*
echo -e $Y"/etc/pacman.d/hooks/sync.hook as done"$DF
echo
sleep 1



#---sensors
echo -e $Y"sensors installing..."$DF
echo
sleep 1
pacman -S --needed --noconfirm lm_sensors
sensors-detect --auto
echo -e $Y"sensors has done"$DF
echo
sleep 1



#---mkinitcpio
#echo -e $Y"mkinitcpio updating..."$DF
#sed -i '52c\HOOKS=(base udev autodetect modconf block keymap filesystems resume keyboard fsck)' /etc/mkinitcpio.conf
#mkinitcpio -p linux-lts
#echo -e $Y"mkinitcpio.conf done"$DF



#---grub
echo -e $Y"grub installing..."$DF
echo
sleep 1
pacman -S --noconfirm --needed grub os-prober
grub-install /dev/sda



#---personalization grub
#sed -i '3c\GRUB_DEFAULT=saved' /etc/default/grub
#sed -i '4c\GRUB_TIMEOUT=1' /etc/default/grub
#sed -i '6c\GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3"' /etc/default/grub
#sed -i '42c\GRUB_COLOR_NORMAL="cyan/black"' /etc/default/grub
#sed -i '43c\GRUB_COLOR_HIGHLIGHT="dark-gray/black"' /etc/default/grub
#sed -i '46c\GRUB_BACKGROUND="/boot/grub/xgrub-img/arch-logo-shiny-dark.png"' /etc/default/grub
#sed -i '54c\GRUB_SAVEDEFAULT=true' /etc/default/grub
#sed -i '57c\GRUB_DISABLE_SUBMENU=y' /etc/default/grub
#sed -i '63c\GRUB_DISABLE_OS_PROBER=true' /etc/default/grub

sed -i '3c\GRUB_DEFAULT=saved' /etc/default/grub
sed -i '4c\GRUB_TIMEOUT=1' /etc/default/grub
sed -i '6c\GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3"' /etc/default/grub
sed -i '42c\GRUB_COLOR_NORMAL="yellow/black"' /etc/default/grub
sed -i '43c\GRUB_COLOR_HIGHLIGHT="black/yellow"' /etc/default/grub
sed -i '46c\GRUB_BACKGROUND="/boot/grub/xgrub-img/grubtux-1.png"' /etc/default/grub
sed -i '54c\GRUB_SAVEDEFAULT=true' /etc/default/grub
sed -i '57c\GRUB_DISABLE_SUBMENU=y' /etc/default/grub
sed -i '63c\GRUB_DISABLE_OS_PROBER=true' /etc/default/grub
chmod -vR 0644 /etc/default/grub

cp -v /etc/grub.d/40_custom /etc/grub.d/40_custom.aux
sed -i '1c\#!/bin/sh' /etc/grub.d/40_custom
sed -i '2c\exec tail -n +3 $0' /etc/grub.d/40_custom
sed -i '5a\ ' /etc/grub.d/40_custom
sed -i '6a\menuentry 'Reboot'{reboot}' /etc/grub.d/40_custom
sed -i '7a\menuentry 'PowerOff'{halt}' /etc/grub.d/40_custom
chmod -v 0644 /etc/grub.d/40_custom

install -v --directory -m 0755 -o root -g root /boot/grub/grub-custom
cp -rv grub-custom/*.png /boot/grub/grub-custom
chmod -vR 0644 /boot/grub/grub-custom/*

#---grub configuration
grub-mkconfig -o /boot/grub/grub.cfg
echo -e $Y"grub has done"$DF
echo
sleep 1



#---networkctl
echo -e $Y"networkctl configurando..."$DF
echo
sleep 1
###WIRED
install -v --directory -m 0755 -o root -g root /etc/systemd/network
cp -vf {33-dhcp-wired.network,33-static-wired.network.bak} \
    /etc/systemd/network
###WIRELESS
cp -vf  {33-dhcp-wifi.network,33-static-wifi.network.bak} \
    /etc/systemd/network
chmod -vR 0644 /etc/systemd/network/*
systemctl disable systemd-networkd.service
systemctl disable systemd-resolved.service



#---networkmanager
echo -e $Y"networkmanager installing..."$DF
echo
sleep 1
pacman -S --needed --noconfirm networkmanager \
    network-manager-applet nm-connection-editor
systemctl disable wpa_supplicant
systemctl enable NetworkManager



#---make skel directory
install -v --directory -m 0755 -o root -g root /etc/skel
echo -e $Y"skel directory has done"$DF
echo
sleep 1



#---make a file to /etc/bashrc
cp -rvf bashrc /etc

echo -e $Y"/etc/bashrc has done"$DF
echo
sleep 1



#---make a file to .xinitrc
cp -vf dot-xinitrc /etc/skel/.xinitrc

cp -vf /etc/skel/.xinitrc /home/${XUSER}
chmod -vR 0644 /home/${XUSER}/
chown -vR ${XUSER}.${XUSER} /home/${XUSER}/
echo -e $Y"~.xinitrc ${XUSER} has done"$DF
echo
sleep 1



#---make a file to /etc/xprofile
cp -vf xprofile /etc
chmod -v 0644 /etc/xprofile
echo -e $Y"/etc/xprofile has done"$DF
echo
sleep 1



#---make a file dot bashrc to /etc/skell directory
mv -v /etc/skel/.bashrc /etc/skel/dot-bashrc.aux
cp -vf dot-bashrc /etc/skel/.bashrc
echo -e $Y"/etc/bashrc has done"$DF
echo
sleep 1
#---copy file from /etc/skel to root directory
cp -vf /etc/skel/.bashrc /root
chmod -v 0700 /root
chmod -v 0644 /root/.bashrc
source /root/.bashrc
echo -e $Y".bashrc root has done"$DF
echo
sleep 1
#---copy file from /etc/skel to user directory
cp -vf /etc/skel/.bashrc /home/${XUSER}
chmod -v 0644 /home/${XUSER}/.*
chown -v ${XUSER}:${XUSER} /home/${XUSER}/.*
echo -e $Y".bashrc ${XUSER} has done"$DF
echo
sleep 1



#---copy file dot bash_profile to /etc/skel directory
cp -vf dot-bash_profile /etc/skel/.bash_profile
chmod -v 0644 /etc/skel/.bash_profile
#---make bash_profile to root directory
cp -vf /etc/skel/.bash_profile /root
chmod -v 0644 /root/.bash_profile
echo -e $Y".bash_profile root has done"$DF
echo
sleep 1
#---copy file from /etc/skel to user directory
cp -vf /etc/skel/.bash_profile /home/${XUSER}
chmod -v 0644 /home/${XUSER}/.bash_profile
chmod -v 0644 /etc/skel/{*,.*}
chown -v ${XUSER}.${XUSER} /home/${XUSER}
echo -e $Y".bash_profile ${XUSER} has done"$DF
echo
sleep 1



#---make a file to /etc/inputrc
cp -vf inputrc /etc
chmod 0644 /etc/inputrc
echo -e $Y"/etc/inputrc has done"$DF
echo
sleep 1



#---profile.d
install -v --directory -m 0755 -o root -g root /etc/profile.d
cp -vf profile-d/*.sh /etc/profile.d
chmod -v 0644 /etc/profile.d/*
echo -e $Y"/etc/profile.d files has done"$DF
echo
sleep 1



#---bash completion
install -v --directory -m 0755 -o root -g root /etc/bash_completion.d
echo -e $Y"/etc/bash_completion.d has done"$DF
echo
sleep 1



#---dircolors
#export LS_OPTIONS='--color=auto'
#eval "$(dircolors -bmycolorscheme)"
#alias ls='ls $LS_OPTIONS'

#echo -e 
#$Y"/etc/dircolors has done"$DF
#echo
#sleep 1



#---vimrc
if [[ ! -x "$(command -v vim)" ]];then
	pacman -S --noconfirm --needed vim
	echo -e $Y"vim installed ok"$DF
fi
cp -vf vimrc /etc
chmod -v 0644 /etc/vimrc
cp -vfr vimcolors/*.vim /usr/share/vim/vim91/colors
chmod -vR 0644 /usr/share/vim/vim91/colors/*
echo -e $Y"vimrc and colors has done"$DF
sleep 1



#---make a .gitconfig to user directory
if [[ ! -f "/home/${XUSER}/.gitconfig" ]];then
	cp -vf dot-gitconfig /home/${XUSER}/.gitconfig
    chmod -v 0644 /home/${XUSER}/.gitconfig
    chown -v ${XUSER}:${XUSER} /home/${XUSER}/.gitconfig
	echo -e $Y"git user has done"$DF
	echo
	sleep 1
fi



#---permissions to arch-init
chown ${XUSER}:${XUSER} /home/${XUSER}/{*,.*}
chown root:root /home
chown -R ${XUSER}:${XUSER} /arch-init



#---notes
echo -e $R'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    	 DONT FORGET

# exit
# umount -Rv /mnt
# reboot

                          <(")
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'$DF



#---remove files and local variables
tar -tf config.tar > config.nfo
xargs rm -rf < config.nfo
rm -rf config.nfo
unset DF R G Y B C XUSER

