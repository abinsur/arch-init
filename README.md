<div align="center">

[<img src="https://i.imgur.com/UjnRFbR.png" width="500">](https://archlinux.org/)

</div>


<div align="center">

Copyright © 2002-2022 Judd Vinet, Aaron Griffin and Levente Polyák.

The [Arch Linux](https://archlinux.org/) name and logo are recognized trademarks. Some rights reserved.

[Linux®](https://www.kernel.org/) is a registered trademark of Linus Torvalds.

</div>