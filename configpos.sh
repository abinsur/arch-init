#!/usr/bin/env bash
# filename: configpos.sh
####################################
### abinsur<at>cryptolab<dot>net ###
####################################



#---colors
DF="\033[0;39m"   #default
R="\033[1;31m"    #redmycolorscheme
G="\033[1;32m"    #green
Y="\033[1;33m"    #yellow
B="\033[1;34m"    #blue
C="\033[0;36m"    #cyan



#---no root only user
if [[ "${EUID}" -eq 0 ]]; then
    echo -e $R"you are ROOT you can not run this app"$DF
    exit
fi



#---untar
tar -xf config.tar



#---timedate local hostname ctl's
XUSER="$(</etc/hostname)"
sudo timedatectl set-ntp true
sudo timedatectl set-timezone "America/Sao_Paulo"
sudo localectl set-locale LANG="pt_BR.UTF-8"
sudo hostnamectl set-hostname "${XUSER}"



#---reflector
if [[ ! -x $(command -v /usr/bin/reflector) ]];then
    sudo pacman -S --noconfirm --needed reflector
fi
sudo mv -v /etc/xdg/reflector/reflector.conf /etc/xdg/reflector/reflector.conf.aux
sudo install -v -m 0644 -o root -g root reflector.conf /etc/xdg/reflector
sudo systemctl enable reflector.timer
echo -e $Y"reflector has done"$DF
sleep 1



#---wallpapers
BG_ABINSUR="/usr/share/abinsur/wallpapers"
if [[ ! -d "${BG_ABINSUR}" ]];then
	sudo install -v -d -m 0755 -o root -g root ${BG_ABINSUR}
    sudo install -v -m 0644 -o root -g root wallpapers/* -t ${BG_ABINSUR}
fi
sleep 1



#---icons
pacman -Sy --noconfirm -needed rsync
ICON_ABINSUR="/usr/share/abinsur/icons"
if [[ ! -d "${ICON_ABINSUR}" ]];then
    sudo install -v -d -m 0755 -o root -g root ${ICON_ABINSUR}
    sudo rsync -vauptog icons ${ICON_ABINSUR}
fi
sleep 1



#---ttf
DIRTTF="/usr/share/fonts/TTF"
if [[ ! -d "${DIRTTF}" ]];then
    sudo install -v -d -m 0755 -o root -g root ${DIRTTF}
fi
sudo install -v -m 0644 -o root -g root ttfs/* -t ${DIRTTF}
sudo fc-cache -fv
echo -e $Y"restored ttf..."$DF
sleep 1



#---gtksourceview
DIRGTK3="/usr/share/gtksourceview-3.0"
if [[ ! -d "${DIRGTK3}" ]];then
    sudo install -v -d -m 0755 -o root -g root styles ${DIRGTK3}
fi
sudo rsync -vauptog styles/ ${DIRGTK3}/styles

DIRGTK4="/usr/share/gtksourceview-4"
if [[ ! -d "${DIRGTK4}" ]];then
	sudo install -v -d -m 0755 -o root -g root ${DIRGTK4}
fi
sudo rsync -vauptog styles/ ${DIRGTK4}/styles
echo -e $Y"restored gtksourceview 3 and 4"$DF
echo
sleep 1



#---openbox themes
sudo cp -rv openbox-themes/* /usr/share/themes
echo -e $Y"openbox themes has done"$DF
echo
sleep 1



#---install lxqt base and other programs
echo -e $Y"installing lxqt personal base"$DF
sleep 1
sudo pacman -S --needed --noconfirm $(<lxqt-app.txt)



#---remove files and local variables
tar -tf config.tar > config.txt
xargs rm -rvf < config.txt
rm -rvf config.txt
unset DF R G Y B C XUSER BG_ABINSUR ICON_ABINSUR DIRTTF DIRGTK3 DIRGTK4

