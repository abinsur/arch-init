#!/usr/bin/env bash
# filename: yaycfg.sh
####################################
### abinsur<at>cryptolab<dot>net ###
####################################



#---colors
DF="\033[0;39m"   #default
R="\033[1;31m"    #red
G="\033[1;32m"    #green
Y="\033[1;33m"    #yellow
B="\033[1;34m"    #blue
C="\033[0;36m"    #cyan



#---user NOT root
if [[ "${EUID}" -eq 0 ]];then
    echo -e $R"it as DO NOT to be root"$DF
    exit
fi



#---untar
tar -xf config.tar



#---var
XUSER="$(</etc/hostname)"



#---yay archlinux aur
if [[ ! -x $(command -v /usr/bin/git) ]];then
    sudo pacman -S --noconfirm --needed git
else
    echo -e $Y"git exist"$DF

fi

if [[ ! -x $(command -v /usr/bin/yay) ]];then
    echo -e $Y"installing yay"$DF
    echo
    sleep 3
    git clone https://aur.archlinux.org/yay.git

    cd yay
    makepkg -si --noconfirm --needed
    yay --combinedupgrade --editmenu --nodiffmenu --save
    echo -e $Y"yay has done"$DF
    echo
    sleep 1

    cd ..
    rm -rf yay
else
    echo $Y"yay exist"$DF
    echo
    sleep 1
fi



#---install i3lock
if [[ ! -x $(command -v /usr/bin/i3lock) ]];then
    yay -Sy --noconfirm --needed i3lock-color
else
    echo -e $Y"i3lock exist"$DF
    echo
	sleep 1
fi



#---add lock command
#sed -i '8a\ ' $HOME/.config/lxqt/lxqt.conf
#sed -i '9a\[Screensaver]' $HOME/.config/lxqt/lxqt.conf
#sed -i '10a\lock_command=i3lock' $HOME/.config/lxqt/lxqt.conf



#---make configuration i3lock
DIR_I3="/usr/share/abinsur/lckscr"
if [[ ! -d "${DIR_I3}" ]];then
    sudo install -v -d -m 0755 -o root -g root ${DIR_I3}
    echo -e $Y"make a directory ${DIR_I3}"$DF
    echo
    sleep 1
else
    echo -e $Y"directory exist ${DIR_I3}"$DF
    echo
    sleep 1
fi

FL_I3C="/usr/share/abinsur/lckscr/i3lock-color"
if [[ ! -f "${FL_I3C}" ]];then
    sudo install -v -m 0644 -o root -g root -t ${FL_I3C}
    echo -e $Y"make a file i3lock-color"$DF
    echo
    sleep 1
else
    echo -e $Y"i3color exist"$DF
    echo
    sleep 1
fi



#---lockscreen
sudo mv -v /usr/share/applications/lxqt-lockscreen.desktop \
/usr/share/applications/lxqt-lockscreen.desktop.aux

sudo install -v -m 0644 -o root -g root lxqt-lockscreen.desktop -t \
/usr/share/applications/lxqt-lockscreen.desktop



#---install aur
yay --needed --noconfirm $(<lxqt-aur-app.txt)



#---remove files
tar -tf config.tar > config.txt
xargs rm -rvf < config.txt
rm -rvf config.txt
unset DF R G Y B C XUSER DIR_I3 FL_I3C


#---suspend
#sudo mv -v /usr/share/applications/lxqt-suspend.desktop \
#/usr/share/applications/lxqt-suspend.desktop.aux



#---leave
#sudo mv -v /usr/share/applications/lxqt-leave.desktop \
#/usr/share/applications/lxqt-leave.desktop.aux



#---logout
#sudo mv -v /usr/share/applications/lxqt-logout.desktop \
#/usr/share/applications/lxqt-logout.desktop.aux



#---hibernate
#sudo mv -v /usr/share/applications/lxqt-hibernate.desktop \
#/usr/share/applications/lxqt-hibernate.desktop.aux



#---shortcut keys
#preferencies--->defenitions lxqt--->shortcuts keys
#cat >> /home/lxqt/.config/lxqt/globalkeyshortcuts.conf << "AUX"
#[Shift%2BControl%2BMeta%2BL.47]
#Comment=i3lock-color
#Enabled=true
#Exec=bash, /home/lxqt/.config/i3lock-color
#AUX



